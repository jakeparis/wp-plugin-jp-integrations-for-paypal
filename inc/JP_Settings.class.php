<?php
namespace JakeParis\PayPayIntegrations;
defined('ABSPATH') || die('Not allowed');


class JP_Settings {

	private static $instance = null;
	private static $optionName = 'jp_paypal';
	
	private static $defaultSettings = array(
		'merchant_id' => '',
		'default_tax_rate' => '',
		'default_success_page' => '',
		'default_cancel_page' => '',
	);

	private static $settings = null;

	static function getSettings ($overrideCache=false) {
		if( self::$settings === null || $overrideCache === true )
			self::$settings = get_option( self::$optionName . "_settings", self::$defaultSettings );
		return self::$settings;
	}
	static function saveSettings( $toSave ) {
		self::$settings = array_merge(
			self::getSettings(), 
			$toSave
		);
		update_option( self::$optionName . "_settings", self::$settings );
	}
	static function deleteSettings () {
		delete_option( self::$optionName . "_settings" );
	}
	static function getSetting ($settingName, $default=false) {
		$settings = self::getSettings();
		if( array_key_exists($settingName, $settings) )
			return $settings[ $settingName ];
		return $default;
	}
	// static function saveSetting ( $settingName, $value ) {
	// 	$settings = self::getSettings();
	// 	$settings[ $settingName ] = $value;
	// 	self::saveSettings( $settings );
	// }

}