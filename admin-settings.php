<?php
namespace JakeParis\PayPayIntegrations;
defined('ABSPATH') || die('Not allowed');

?>
<style>
	label.block {
		display: block;
		font-weight: bold;
		margin: .7em 0 .2em;
		font-size: 1.1em;
	}
	.help {
		color: hsl(0, 0%, 40%);
		font-style: italic;
		margin: 0;
		padding: .4em 0;
	}
</style>
<div class="wrap">
	
	<h1>PayPal Integrations</h1>

	<?php
	if( isset($_POST['save-settings']) ) :
		if( ! wp_verify_nonce( $_POST['_jp-paypalintegrations-settings-nonce'], 'save-jp-paypalintegrations-settings' ) )
				echo '<div class="notice notice-error"><p>There was a problem. Perhaps the form timed out?</p></div>';
			else {
				JP_Settings::saveSettings([
					'merchant_id' => $_POST['merchant_id'],
					'default_success_page' => $_POST['default_success_page'],
					'default_cancel_page' => $_POST['default_cancel_page'],
					'default_tax_rate' => str_replace('%', '', $_POST['default_tax_rate']),
				]);

				echo '<div class="notice notice-success"><p>Saved</p></div>';
			}
	endif;

	$settings = JP_Settings::getSettings();
	?>


	<form action="" method="post">
		<?php
		wp_nonce_field( 'save-jp-paypalintegrations-settings', '_jp-paypalintegrations-settings-nonce');
		?>

		<p>Create "Buy Now" or "Pay Now" buttons to sell single items on PayPal. After creating these settings, you can insert blocks anywhere you'd like.</p>

		<label class="block">Merchant ID</label>
		<p class="help">Enter a valid Merchant account ID (strongly recommend) or PayPal account email address. All payments will go to this account.
		</p>
		<p class="help">You can find your Merchant account ID in your PayPal account under Profile -> My business info -> Merchant account ID.</p>
		<p class="help">If you don't have a PayPal account, you can sign up for free at <a target='_blank' href='https://paypal.com'>PayPal</a>.</p>
		<input type="text" name="merchant_id" value="<?= esc_attr( $settings['merchant_id'] ) ?>">

		<label class="block">Default Tax Rate</label>
		<p class="help">Optional</p>
		<input type="text" name="default_tax_rate" value="<?= esc_attr( $settings['default_tax_rate'] ) ?>" placeholder="for example, 5.5">%

		<label class="block">Success page</label>
		<p class="help">(optional) Send users to this page after a successful transaction.</p>
		<?php
		wp_dropdown_pages([
			'selected' => $settings['default_success_page'],
			'name' => 'default_success_page',
			'show_option_none' => ' -- ',
		]);
		?>
		<label class="block">Cancel page</label>
		<p class="help">(optional) Send users to this page if they cancel the transaction.</p>
		<?php
		wp_dropdown_pages([
			'selected' => $settings['default_cancel_page'],
			'name' => 'default_cancel_page',
			'show_option_none' => ' -- ',
		]);
		?>

		<?php 
		submit_button( 'Save', 'primary', 'save-settings' );
		?>
	</form>

</div>
