# JP Integrations for Paypal #
**Contributors:** jakeparis  
**Donate link:** https://jake.paris/  
**Tags:** PayPal payment, PayPal, button, payment, online payments, pay now, buy now, ecommerce, gateway, paypal button, paypal buy now button, paypal plugin  
**Requires at least:** 5.4  
**Tested up to:** 5.8.2  
**Stable tag:** 1.0.0  

Provide blocks to easily create "Pay Now/Buy Now" buttons on PayPal.com.

## Description ##

Provides Gutenberg blocks to easily create "Pay Now/Buy Now" buttons on PayPal.com. You will need to have a paypal account. Tip of the hat to Scott Paterson for the inspiration.


## Changelog ##

### 1.3.1 ###
Test only, no changes.

### 1.3.0 ###
Updated the updater path.

### 1.2.1 ###
Fix the permission name for settings page to fix issue where the settings page fails to show in some cases.

### 1.2.0 ###

Add option to get url for payment rather than form with hidden fields.

### 1.1.0 ###

Added Item name and Item ID.

### 1.0.0 ###

First version.



## Frequently Asked Questions ##

### Do I need to have a PayPal account? ###

Yes you do. You can sign up for a free account at [Paypal.com](https://paypal.com).

