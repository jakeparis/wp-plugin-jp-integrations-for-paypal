<?php
namespace JakeParis\PayPayIntegrations;
/*
Plugin Name: JP Integrations for Paypal
Plugin URI:  https://gitlab.com/jakeparis/wp-plugin-jp-integrations-for-paypal
Description: Adds PayPal integrations, such as Pay Now buttons. 
Version:     1.3.1
Author:      Jake Paris
Author URI:  https://jake.paris/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
/*  Copyright 2021 Jake Paris

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

define('JP_PAYPAL_PLUGIN_VERSION', '1.3.1');
define('JP_PAYPAL_PLUGIN_URL', plugins_url( '/', __FILE__));
define('JP_PAYPAL_PLUGIN_PATH', plugin_dir_path(__FILE__));

foreach (glob( JP_PAYPAL_PLUGIN_PATH . "inc/*.php") as $file) {
	require_once $file;
}
require_once JP_PAYPAL_PLUGIN_PATH . 'editor/registration.php';

add_action('admin_menu', function(){
	add_options_page( 'PayPal Integrations', 'PayPal Integrations', 'update_themes', 'jp-paypal-integrations', __NAMESPACE__.'\settingsPage' );
});

function settingsPage(){
	require_once JP_PAYPAL_PLUGIN_PATH . 'admin-settings.php'; 
}


register_deactivation_hook( __FILE__, function(){
	JP_Settings::deleteSettings();
});

/**
 * Updater
 */
require plugin_dir_path(__FILE__) . 'updater/plugin-update-checker.php';
$myUpdateChecker = \Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/wp-plugin-jp-integrations-for-paypal',
	__FILE__, //Full path to the main plugin file or functions.php.
	'jp-integrations-for-paypal/jp-integrations-for-paypal.php'
);
