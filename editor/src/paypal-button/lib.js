

export function getButtonImage (attributes) {
	if( attributes.button_type === 'custom' )
		return false;
	const verb = attributes.button_text === 'Buy Now' ? 'buy' : 'pay';
	const buttons = {
		small: {
			classic: `https://www.paypalobjects.com/en_US/i/btn/btn_${verb}now_SM.gif`,
			new: `https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-small.png`,
		},
		medium: {
			classic: '', // they don't make this
			new: `https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-medium.png`
		},
		large: {
			classic: `https://www.paypalobjects.com/en_US/i/btn/btn_${verb}now_LG.gif`,
			new: `https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png`,
		},
		with_cards: `https://www.paypalobjects.com/en_US/i/btn/btn_${verb}nowCC_LG.gif`,
	};

	var imageUrl;
	if( attributes.display_cards )
		imageUrl = buttons.with_cards;
	else {
		let buttonStyle = (attributes.include_logo) ? 'new' : 'classic';
		imageUrl = buttons[ attributes.button_size ][ buttonStyle ];
	}
	return imageUrl;
};

export function makePayLink (attributes) {

	const baseurl = 'https://www.paypal.com/cgi-bin/webscr';
	var params = {
		cmd: "_xclick",
		business: attributes.merchant_id,
		lc: 'US',
		amount: attributes.amount,
		currency_code: "USD",
		button_subtype: "services",
		rm: 1
	};

	if( attributes.allow_special_instructions ) {
		params.no_note = "0";
		params.cn = "Add special instructions to the seller:";
	}

	if( ! attributes.allow_special_instructions )
		params.no_note = "1";

	if( attributes.default_tax_rate )
		params.tax_rate = attributes.default_tax_rate;

	if( attributes.item_name )
		params.item_name = attributes.item_name;

	if( attributes.item_id )
		params.item_number = attributes.item_id;

	if( attributes.ask_for_address )
		params.no_shipping = "2";

	if( ! attributes.ask_for_address )
		params.no_shipping = "1";

	if( attributes.default_success_page )
		params.return = attributes.default_success_page;

	if( attributes.default_cancel_page )
		params.cancel_return = attributes.default_cancel_page;

	if( attributes.shipping_amount )
		params.shipping = attributes.shipping_amount;

	params = new URLSearchParams( params );

	return baseurl + '?' + params.toString();
}