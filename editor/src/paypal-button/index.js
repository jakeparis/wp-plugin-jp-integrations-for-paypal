
import edit from './edit';
import save from './save';
import icon from './icon';

import blockData from './block.json';

import {
	registerBlockType,
} from '@wordpress/blocks';

registerBlockType( blockData.name, {
	...blockData,
	icon,
	edit,
	save,
});
